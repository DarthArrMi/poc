package com.globant.poc.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.globant.poc.R;
import com.globant.poc.app.AppSettings;
import com.globant.poc.service.WebServerService;
import com.globant.poc.utils.Utils;

public class WebServerActivity extends Activity {

    private Button mButtonServerActions;
    private TextView mTextStatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mTextStatus = (TextView) findViewById(R.id.textStatus);
        mButtonServerActions = (Button) findViewById(R.id.buttonServerActions);
        mButtonServerActions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WebServerActivity.this, WebServerService.class);

                if (AppSettings.isServiceStarted(WebServerActivity.this)) {
                    stopService(intent);

                    AppSettings.setServiceStarted(WebServerActivity.this, false);
                    setButtonText(false);
                    setInfoText(false);
                } else {
                    startService(intent);

                    AppSettings.setServiceStarted(WebServerActivity.this, true);
                    setButtonText(true);
                    setInfoText(true);
                }
            }
        });

        boolean isRunning = AppSettings.isServiceStarted(this);

        setButtonText(isRunning);
        setInfoText(isRunning);
    }

    private void setButtonText(boolean isServiceRunning) {
        mButtonServerActions.setText(getString(isServiceRunning ? R.string.text_stop_server : R.string.text_start_server));
    }

    private void setInfoText(boolean isServiceRunning) {
        String text = getString(R.string.text_log_not_running);

        if (isServiceRunning) {
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);

            text = getString(R.string.text_log_running, Utils.getIpv4Address() + ":8080");
        }

        mTextStatus.setText(text);
    }
}