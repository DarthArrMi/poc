package com.globant.poc.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.globant.poc.R;
import com.globant.poc.server.WebServer;
import com.globant.poc.ui.WebServerActivity;

public class WebServerService extends Service {

    private NotificationManager notificationManager;

    private WebServer webServer;

    private static final int NOTIFICATION_ID = 666;

    @Override
    public void onCreate() {
        super.onCreate();

        webServer = new WebServer(this);
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public void onDestroy() {
        webServer.stopServer();
        notificationManager.cancel(NOTIFICATION_ID);
        notificationManager = null;

        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        webServer.startServer();
        showNotification();

        return START_STICKY;
    }

    private void showNotification() {
        String text = getString(R.string.text_service_started);
        Notification notification = new Notification(R.mipmap.ic_launcher, text, System.currentTimeMillis());

        Intent startIntent = new Intent(this, WebServerActivity.class);

        startIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(this, 0, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        notification.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;

        notification.setLatestEventInfo(this,
                getString(R.string.notification_title_server_started),
                getString(R.string.notification_description_server_started),
                intent);


        notificationManager.notify(NOTIFICATION_ID, notification);
    }
}
