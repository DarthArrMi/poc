package com.globant.poc.server;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.globant.poc.constants.Constants;

import org.apache.http.HttpException;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.DefaultHttpResponseFactory;
import org.apache.http.impl.DefaultHttpServerConnection;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.BasicHttpProcessor;
import org.apache.http.protocol.HttpRequestHandlerRegistry;
import org.apache.http.protocol.HttpService;
import org.apache.http.protocol.ResponseConnControl;
import org.apache.http.protocol.ResponseContent;
import org.apache.http.protocol.ResponseDate;
import org.apache.http.protocol.ResponseServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer extends Thread {

    private boolean isRunning = false;
    private int serverPort = 0;

    private Context context;

    private BasicHttpProcessor httpProcessor;
    private BasicHttpContext httpContext;
    private HttpService httpService;
    private HttpRequestHandlerRegistry handlerRegistry;

    private static final String SERVER_NAME = "Hermetic Server POC";
    private static final String PATTERN_ALL = "*";

    public WebServer(Context context) {
        super(SERVER_NAME);

        this.setContext(context);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);

        serverPort = Integer.parseInt(pref.getString(Constants.PREF_SERVER_PORT, "" + Constants.DEFAULT_SERVER_PORT));

        httpContext = new BasicHttpContext();

        httpProcessor = new BasicHttpProcessor();
        httpProcessor.addInterceptor(new ResponseDate());
        httpProcessor.addInterceptor(new ResponseServer());
        httpProcessor.addInterceptor(new ResponseContent());
        httpProcessor.addInterceptor(new ResponseConnControl());

        httpService = new HttpService(httpProcessor,
                new DefaultConnectionReuseStrategy(),
                new DefaultHttpResponseFactory());

        handlerRegistry = new HttpRequestHandlerRegistry();
        handlerRegistry.register(PATTERN_ALL, new IndexHandler(context));

        httpService.setHandlerResolver(handlerRegistry);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public void run() {
        super.run();

        try {
            ServerSocket serverSocket = new ServerSocket(serverPort);
            serverSocket.setReuseAddress(true);

            while (isRunning) {
                Socket socket = serverSocket.accept();

                DefaultHttpServerConnection serverConnection = new DefaultHttpServerConnection();
                serverConnection.bind(socket, new BasicHttpParams());
                httpService.handleRequest(serverConnection, httpContext);

                serverConnection.shutdown();
            }

            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (HttpException e) {
            e.printStackTrace();
        }
    }

    public synchronized void startServer() {
        isRunning = true;

        super.start();
    }

    public synchronized void stopServer() {
        isRunning = false;
    }
}
